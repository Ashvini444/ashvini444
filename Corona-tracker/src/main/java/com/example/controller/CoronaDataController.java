package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.model.LocationStats;
import com.example.service.CoronaDataService;

@Controller
public class CoronaDataController {

    @Autowired
   CoronaDataService cds;

    @RequestMapping(path="/get", method = RequestMethod.GET)
    public String home(Model model)  {
    	//model.addAttribute("testName","Test");
    	List<LocationStats> allStats = cds.getAllStats();
         int totalReportedCases = allStats.stream().mapToInt(stat -> stat.getLatestTotalCases()).sum();
         int totalNewCases = allStats.stream().mapToInt(stat -> stat.getDiffFromPrevDay()).sum();
         model.addAttribute("locationStats", allStats);
         model.addAttribute("totalReportedCases", totalReportedCases);
         model.addAttribute("totalNewCases", totalNewCases);
        return "home";
    }
}
