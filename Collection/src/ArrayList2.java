import java.util.ArrayList;
import java.util.List;  
class Subject {  
int id;  
String name,teacher;  
int quantity;  
public Subject(int id, String name, String teacher) {  
    this.id = id;  
    this.name = name;  
    this.teacher = teacher;  
    
}  
}  
public class ArrayList2{  
public static void main(String[] args) {  
    //Creating list of Subjects 
    List<Subject> list=new ArrayList<Subject>();  
    //Creating Books  
    Subject s1=new Subject(101,"Machine Learning","Trupti Suryawanshi");  
    Subject s2=new Subject(102,"Cloud Computing","Nadif Sir");  
    Subject s3=new Subject(103,"ERTOS","Lomte Sir");  
    //Adding Books to list  
    list.add(s1);  
    list.add(s2);  
    list.add(s3);  
    //Traversing list  
    for(Subject s:list){  
        System.out.println(s.id+" "+s.name+" "+s.teacher);  
    }  }
}  