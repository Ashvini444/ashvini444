package com.example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.google.i18n.phonenumbers.NumberParseException; 
import com.google.i18n.phonenumbers.PhoneNumberUtil; 
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber; 

public class Mobile { 

	public void MB() throws IOException
	{ 
		
		
        // creating an array of random phone numbers 
        String[] phonenumbers 
            = { "+91 94483 76473", "1800 425 3800", 
                "+91 83944 7484", "0294 2424447" }; 
  
        // iterating over each number to validate 
        for (String phone : phonenumbers) { 
            if (isPhoneNumberValid(phone)) { 
                System.out.println(phone + " is valid."); 
            } 
            else { 
                System.out.println(phone 
                                   + " is not valid."); 
            } 
        } 
    } 
  
    // this method return true if the passed phone number is 
    // valid as per the region specified 
    public static boolean isPhoneNumberValid(String phone) 
    { 
        // creating an instance of PhoneNumber Utility class 
        PhoneNumberUtil phoneUtil 
            = PhoneNumberUtil.getInstance(); 
        
        // creating a variable of type PhoneNumber 
        PhoneNumber phoneNumber = null; 
  
        try { 
            // the parse method parses the string and 
            // returns a PhoneNumber in the format of 
            // specified region 
            phoneNumber = phoneUtil.parse(phone, "IN"); 
            
            // this statement prints the type of the phone 
            // number 
            System.out.println( 
                "\nType: "
                + phoneUtil.getNumberType(phoneNumber)); 
        } 
        catch (NumberParseException e) { 
            
            // if the phoneUtil is unable to parse any phone 
            // number an exception occurs and gets caught in 
            // this block 
            System.out.println( 
                "Unable to parse the given phone number: "
                + phone); 
            e.printStackTrace(); 
        } 
  
        // return the boolean value of the validation 
        // performed 
        return phoneUtil.isValidNumber(phoneNumber); 
    } 
}

