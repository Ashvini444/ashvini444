Spring Data JPA - Reference
Documentation
Oliver Gierke, Thomas Darimont, Christoph Strobl, Mark Paluch
Version 1.10.11.RELEASE, 2017-06-07
javawithease.com
Table of Contents
Preface 2
1. Project metadata . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 3
2. New & Noteworthy . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 4
2.1. What’s new in Spring Data JPA 1.10 . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 4
3. Dependencies . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 5
3.1. Dependency management with Spring Boot . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 6
3.2. Spring Framework . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 6
4. Working with Spring Data Repositories . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 7
4.1. Core concepts . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 7
4.2. Query methods . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 9
4.3. Defining repository interfaces . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 10
4.3.1. Fine-tuning repository definition. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 11
4.3.2. Using Repositories with multiple Spring Data modules . . . . . . . . . . . . . . . . . . . . . . . . . . . . 11
4.4. Defining query methods. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 14
4.4.1. Query lookup strategies . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 15
4.4.2. Query creation . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 15
4.4.3. Property expressions. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 16
4.4.4. Special parameter handling. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 17
4.4.5. Limiting query results. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 18
4.4.6. Streaming query results . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 18
4.4.7. Async query results . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 19



Chapter 2. New & Noteworthy
2.1. What’s new in Spring Data JPA 1.10
• Support for Projections in repository query methods.
• Support for Query by Example.
• The following annotations have been enabled to build own, composed annotations:
@EntityGraph, @Lock, @Modifying, @Query, @QueryHints and @Procedure.
• Support for Contains keyword on collection expressions.
• AttributeConverters for ZoneId of JSR-310 and ThreeTenBP.
• Upgrade to Querydsl 4, Hibernate 5, OpenJPA 2.4 and EclipseLink 2.6.1.
4
javawithease.com
Chapter 3. Dependencies
Due to different inception dates of individual Spring Data modules, most of them carry different
major and minor version numbers. The easiest way to find compatible ones is by relying on the
Spring Data Release Train BOM we ship with the compatible versions defined. In a Maven project
you’d declare this dependency in the <dependencyManagement /> section of your POM:
Example 1. Using the Spring Data release train BOM
<dependencyManagement>
<dependencies>
<dependency>
<groupId>org.springframework.data</groupId>
<artifactId>spring-data-releasetrain</artifactId>
<version>${release-train}</version>
<scope>import</scope>
<type>pom</type>
</dependency>
</dependencies>
</dependencyManagement>
The current release train version is Hopper-SR11. The train names are ascending alphabetically and
currently available ones are listed here. The version name follows the following pattern: ${name}-
${release} where release can be one of the following:
• BUILD-SNAPSHOT - current snapshots
• M1, M2 etc. - milestones
• RC1, RC2 etc. - release candidates
• RELEASE - GA release
• SR1, SR2 etc. - service releases
A working example of using the BOMs can be found in our Spring Data examples repository. If
that’s in place declare the Spring Data modules you’d like to use without a version in the
<dependencies /> block.
Example 2. Declaring a dependency to a Spring Data module
<dependencies>
<dependency>
<groupId>org.springframework.data</groupId>
<artifactId>spring-data-jpa</artifactId>
</dependency>
<dependencies>
5
javawithease.com
3.1. Dependency management with Spring Boot
Spring Boot already selects a very recent version of Spring Data modules for you. In case you want
to upgrade to a newer version nonetheless, simply configure the property spring-datareleasetrain.
version to the train name and iteration you’d like to use.
3.2. Spring Framework
The current version of Spring Data modules require Spring Framework in version 4.2.9.RELEASE or
better. The modules might also work with an older bugfix version of that minor version. However,
using the most recent version within that generation is highly recommended.
6