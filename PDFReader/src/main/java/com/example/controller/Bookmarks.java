package com.example.controller;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.List;

import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDMetadata;
import org.apache.pdfbox.util.PDFTextStripper;
import org.apache.pdfbox.util.Splitter;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.sax.BodyContentHandler;

import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.canvas.parser.PdfTextExtractor;
import com.itextpdf.kernel.pdf.canvas.parser.listener.SimpleTextExtractionStrategy;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.codec.Base64.InputStream;

//import org.xml.sax.SAXException;


public class Bookmarks {
	
   public void extractMetadata() throws IOException, TikaException, Exception {
	
	   
	   { File file = new File("./src/main/resources/Encapsulation.pdf");
	     PDDocument document = PDDocument.load(file); 

	     //Instantiating Splitter class
	     Splitter splitter = new Splitter();

	     //splitting the pages of a PDF document
	     List<PDDocument> Pages = splitter.split(document);

	     //Creating an iterator 
	     Iterator<PDDocument> iterator = Pages.listIterator();

	     //Saving each page as an individual document
	     int i=1 ;
	     while(iterator.hasNext()) {
	        PDDocument pd = iterator.next();
	        pd.save("./src/main/resources/Encapsulation.pdf"+ i++ +".pdf");
	     }
	     System.out.println("Multiple PDF’s created");
	     document.close();
	  }

}}