package com.example;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.pdf.PDFParser;
import org.apache.tika.sax.BodyContentHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.controller.Bookmarks;
import com.example.controller.ExtractBookmarkedSection;
import com.lowagie.text.pdf.PdfReader;


@SpringBootApplication
public class PdfReaderApplication {

	public static void main(String[] args) throws TikaException, Exception {
		SpringApplication.run(PdfReaderApplication.class, args);
		//PDFModify pm=new PDFModify();
		//pm.modify();
		
		//ShrinkPDF p=new ShrinkPDF();
		//p.Shrink();
		long start =System.currentTimeMillis();
		
PDFToConsole pc=new PDFToConsole();
	pc.showMetadata();
	System.out.println("\n\n-------Bookmarks of PDF are-----------\n");
	pc.getBookmark();
	pc.showContents();
	System.out.println(String.format("\n\n--------Processing took ----------\n%s millisecond\n\n", System.currentTimeMillis()-start));		   

	
//////
//	Bookmarks bk=new Bookmarks();
//	bk.extractMetadata();
//	
//	ExtractBookmarkedSection ebs=new ExtractBookmarkedSection();
//	ebs.getBookmarkSection();
//	bk.getPageContent(10);
	}
	}

