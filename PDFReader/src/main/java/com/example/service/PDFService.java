package com.example.service;

import java.io.FileInputStream;
import java.io.IOException;

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.pdf.PDFParser;
import org.apache.tika.sax.BodyContentHandler;
import org.springframework.stereotype.Service;
@Service
public class PDFService {

	public  void extractfromFile() throws IOException,TikaException, Exception{
		
		long start =System.currentTimeMillis();
		BodyContentHandler handler= new BodyContentHandler(-1);
		Metadata metadata = new Metadata();
		FileInputStream content =new FileInputStream("./src/main/resources/Encapsulation.pdf");
	    ParseContext pcontext = new ParseContext();

		PDFParser pdfparser = new PDFParser();
	    pdfparser.parse(content, handler, metadata,pcontext);
	    
	    
	    System.out.println("--------------------Metadata of the PDF---------------------------\n");
	    String[] metadataNames = metadata.names();
	    
	    for(String name : metadataNames) {
	        System.out.println(name+ " : " + metadata.get(name));
	     }
	    
	    
	    //getting the content of the document
	    System.out.println("\n-------------------Contents of the PDF --------------------\n" + handler.toString());
	    
		
		System.out.println(String.format("--------Processing took ----------\n%s millisecond\n\n", System.currentTimeMillis()-start));
		}

	
}
