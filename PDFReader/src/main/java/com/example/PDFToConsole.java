package com.example;

import java.io.File;
//import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.annotation.Target;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.util.PDFTextStripper;
import org.apache.pdfbox.util.Splitter;


//import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.pdf.PDFParser;
import org.apache.tika.sax.BodyContentHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.service.PDFService;
import com.itextpdf.text.Document;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.SimpleBookmark;
import java.io.File; 
import java.io.IOException; 
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.Iterator;
public class PDFToConsole {
	

		
		public  void showMetadata() throws IOException,TikaException, Exception{
		      File file = new File("./src/main/resources/python_book.pdf");

		      //long start =System.currentTimeMillis();
		     
			BodyContentHandler handler= new BodyContentHandler(-1);
			Metadata metadata = new Metadata();
			FileInputStream content =new FileInputStream(file);
		    ParseContext pcontext = new ParseContext();

			PDFParser pdfparser = new PDFParser();
		    pdfparser.parse(content, handler, metadata,pcontext);
		    System.out.println("\n\n--------------------Metadata of the PDF---------------------------\n");
		    String[] metadataNames = metadata.names();
		    
	    for(String name : metadataNames) {
	        System.out.println(name+ " : " + metadata.get(name));
		     }
	    }
	    public void showContents() throws IOException {
	    	
	    	System.out.println("\n\n------------------------Content of PDF-------------------------\n");
			  File file = new File("./src/main/resources/python_book.pdf");
		      PDDocument document = PDDocument.load(file);
		      PDDocumentCatalog catalog = document.getDocumentCatalog();
		      //PDMetadata metadata = catalog.getMetadata();
		      //Metadata metadata = new Metadata();
		      //Instantiate PDFTextStripper class
		      PDFTextStripper pdfStripper = new PDFTextStripper();
		      
			    //Retrieving text from PDF document

		      String text = pdfStripper.getText(document);
		      System.out.println(text);
		      
		      PdfReader reader = new PdfReader(new FileInputStream(new File("./src/main/resources/python_book.pdf")));   
			     int noPages = reader.getNumberOfPages(); 
			     System.out.println("\nTotal number of pages in pdf are:"+noPages);

				  //  long start =System.currentTimeMillis();

			   
	    }
//		    
		    
		

		public void getBookmark() throws Exception {
		    PdfReader reader = new PdfReader("python_book.pdf");
		    List list = SimpleBookmark.getBookmark(reader);
		    for (Iterator i = list.iterator(); i.hasNext();) {
		      showBookmark((Map) i.next());
		    }
		   }
			
		  private static void showBookmark(Map bookmark) throws FileNotFoundException, IOException {
		    System.out.println(bookmark.get("Title"));
		    ArrayList kids = (ArrayList) bookmark.get("Kids");
		    if (kids == null)
		      return;
		    for (Iterator i = kids.iterator(); i.hasNext();) {
		      showBookmark((Map) i.next());
		    }

			
		  }
		   }

		