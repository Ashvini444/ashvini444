package com.example.service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.Customer;
import com.example.repository.CustomerRepo;
@Service
public class CustomerService {
	
	@Autowired
	private CustomerRepo customerRepo;
	String line="";

	public void saveCustomerData() throws IOException  {
		
		BufferedReader br =new BufferedReader(new FileReader("src/main/resources/CustomerData.csv"));
		while((line=br.readLine())!=null) {
			String [] data= line.split(",");
			Customer c =new Customer();
			c.setName(data[0]);
			c.setIssue(data[1]);
			c.setIssueId(data[2]);
			customerRepo.save(c);
			
			
			
		}
	}

}
