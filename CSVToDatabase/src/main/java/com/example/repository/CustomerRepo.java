package com.example.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.model.Customer;
@Repository
public interface CustomerRepo extends CrudRepository<Customer, Integer>{

}
