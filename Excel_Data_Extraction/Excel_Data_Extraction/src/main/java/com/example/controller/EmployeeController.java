package com.example.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.EmployeeData;
import com.example.service.EmployeeServiceImpl;

@Controller
public class EmployeeController {
	@Autowired
	private EmployeeServiceImpl employeeService;

	@GetMapping("/get")
	public String getData(Model model) throws IOException {
		
		model.addAttribute("employee", new EmployeeData());
		List<EmployeeData> employee1 = employeeService.getData();
		model.addAttribute("employee1", employee1);
		return "Excel_Data";
//	}
//	@RequestMapping("/getEmployee")
//    public String viewHomePage(Model model) throws IOException {
//        List<EmployeeData> employeeList = employeeService.getData();
//        model.addAttribute("employee1", employeeList);
//        return "Excel_Data";
   }
	
}
	
