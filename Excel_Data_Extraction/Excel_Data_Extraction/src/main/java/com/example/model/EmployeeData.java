package com.example.model;

import java.awt.Image;

public class EmployeeData {
	private String sr_No;
	private String emp_Id;
	private String emp_Name;
	private String reward_Type;
	//private String dept;
	private Object image;
	public String getSr_No() {
		return sr_No;
	}
	public void setSr_No(String sr_No) {
		this.sr_No = sr_No;
	}
	public String getEmp_Id() {
		return emp_Id;
	}
	public void setEmp_Id(String emp_Id) {
		this.emp_Id = emp_Id;
	}
	public String getEmp_Name() {
		return emp_Name;
	}
	public void setEmp_Name(String emp_Name) {
		this.emp_Name = emp_Name;
	}
	
	public String getReward_Type() {
		return reward_Type;
	}
	public void setReward_Type(String reward_Type) {
		this.reward_Type = reward_Type;
	}
	public Object getImage() {
		return image;
	}
	public void setImage(Object image) {
		this.image = image;
	}
	
	
   
}
