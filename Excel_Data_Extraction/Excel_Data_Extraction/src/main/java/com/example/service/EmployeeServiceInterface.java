package com.example.service;

import java.io.IOException;
import java.util.List;

import com.example.model.EmployeeData;

public interface EmployeeServiceInterface {
	
	List<EmployeeData> getData() throws IOException;
	
}
