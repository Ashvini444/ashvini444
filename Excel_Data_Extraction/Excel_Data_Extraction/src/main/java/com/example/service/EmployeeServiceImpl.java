package com.example.service;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import com.example.model.EmployeeData;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.ImageIO;

@Service
public class EmployeeServiceImpl implements EmployeeServiceInterface {

    @Override
    public  List<EmployeeData> getData() throws IOException{

    	List<EmployeeData> listEmployee= new ArrayList<EmployeeData>();
  	    HashMap<String,byte[]> map1 = new HashMap<String, byte[]>();

  	    
  	    
  	    
    	FileInputStream excelFile = new FileInputStream(new File("C:\\Users\\ashwi\\Desktop\\Dec.xlsx"));
            Workbook workbook = new XSSFWorkbook(excelFile);
            Sheet datatypeSheet = workbook.getSheetAt(0);
            Iterator<Row> rows = datatypeSheet.iterator();
        //  rows.next();
            
            
            
            
            File file = new File("C:\\Users\\ashwi\\Desktop\\Photos"); //image file path
            for(File fileimg :file.listFiles()) {
            	if(fileimg.getName().endsWith(".jpg")) {
            		FileInputStream fis= new FileInputStream(fileimg);
          	      ByteArrayOutputStream bos = new ByteArrayOutputStream();
        	      byte [] data = new byte[1024];
        	      for(int readNum ;(readNum=fis.read(data))!= -1;) {
        	    	  bos.write(data, 0, readNum);
        	      }
        	      byte[] bytes=bos.toByteArray();  
        	    map1.put(fileimg.getName().replace(".jpg", ""), bytes);
           	 }
     }

            while (rows.hasNext()) {

                Row row = rows.next();
            	EmployeeData employee1 =new EmployeeData();
            	
            	String emp_Id = null;

            	 if (row.getCell(0).getCellType() == CellType.STRING) {
            	        
                    	employee1.setSr_No(row.getCell(0).getStringCellValue());
                     }
            	 if (row.getCell(1).getCellType() == CellType.STRING) {
            	emp_Id = row.getCell(1).getStringCellValue();
            	employee1.setEmp_Id(emp_Id);
            	}
                  
                    if (row.getCell(2).getCellType() == CellType.STRING) {
                    	employee1.setEmp_Name(row.getCell(2).getStringCellValue());
                    	}
                    	 if (row.getCell(3).getCellType() == CellType.STRING) {
                         	employee1.setReward_Type(row.getCell(3).getStringCellValue());
                         }
                    	 if (row.getCell(4) == null || row.getCell(4).getCellType() == CellType.ERROR) {

               		   if (map1.containsKey(emp_Id)) {
                  	   Base64.Encoder encoder = Base64.getEncoder();
                         employee1.setImage(Base64.getEncoder().encodeToString((byte[]) map1.get(emp_Id)));

                     }
                    	 
                      }
                     	 
                         listEmployee.add(employee1);

            }

			return listEmployee;
 }
    
}
               	 
                    	 
                    	 
                    	 
                    	 
                    	 
                          