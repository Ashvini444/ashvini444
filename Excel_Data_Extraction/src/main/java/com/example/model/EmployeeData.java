package com.example.model;

import java.awt.Image;

public class EmployeeData {
	
	private String id;
	private String name;
	private String contact;
	private String dept;
	private byte[] image;
	public byte[] getImage() {
		return image;
	}
	public void setImage(byte[] bytes) {
		this.image = bytes;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	
	
   
}
