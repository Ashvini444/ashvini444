package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExcelDataExtractionApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExcelDataExtractionApplication.class, args);
		
	}

}
