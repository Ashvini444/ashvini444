package com.example.service;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import com.example.model.EmployeeData;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.ImageIO;

@Service
public class EmployeeServiceImpl implements EmployeeServiceInterface {

    @Override
    public  List<EmployeeData> getData() throws IOException{

    	List<EmployeeData> listEmployee= new ArrayList<EmployeeData>();
    	
    	
    	 HashMap<String,EmployeeData> map = new HashMap<String, EmployeeData>();
         for(EmployeeData employee: listEmployee)
         {
         map.put(employee.getId(), employee);
         System.out.println(employee.getId()+"  "+employee.getName());
         
         }
         for(Map.Entry<String, EmployeeData> entry:map.entrySet())
         {    
             String key=entry.getKey();  
             EmployeeData empData=entry.getValue();  
             
            // System.out.println(empData.getId()+" "+empData.getName()+" "+empData.getContact()+" "+empData.getDept());   
          }
         
         
    	
    	FileInputStream excelFile = new FileInputStream(new File("src/main/resources/Book2.xlsx"));
            Workbook workbook = new XSSFWorkbook(excelFile);
            Sheet datatypeSheet = workbook.getSheetAt(0);
            Iterator<Row> rows = datatypeSheet.iterator();
        //  rows.next();
            while (rows.hasNext()) {

                Row row = rows.next();
            	EmployeeData employee1 =new EmployeeData();
            	 
                if (row.getCell(0).getCellType() == CellType.STRING) {

                    	employee1.setId(row.getCell(0).getStringCellValue());
                    }
                   if (row.getCell(1).getCellType() == CellType.STRING) {
        
                   	employee1.setName(row.getCell(1).getStringCellValue());
                    }
                    if (row.getCell(2).getCellType() == CellType.STRING) {
                    	employee1.setContact(row.getCell(2).getStringCellValue());
                    }
                	 if (row.getCell(3).getCellType() == CellType.STRING) {
                     	employee1.setDept(row.getCell(3).getStringCellValue());
                     }
                	 
                    	 File file = new File("src/main/resources/Photos"); //image file path
                         for(File fileimg :file.listFiles()) {
                         	if(fileimg.getName().endsWith(".jpg")) {
                         		FileInputStream fis= new FileInputStream(fileimg);
                       	      ByteArrayOutputStream bos = new ByteArrayOutputStream();
                     	      byte [] data = new byte[1024];
                     	      for(int readNum ;(readNum=fis.read(data))!= -1;) {
                     	    	  bos.write(data, 0, readNum);
                     	      }
                     	      byte[] bytes=bos.toByteArray();
                     	    	  
                     	    HashMap<String,byte[]> map1 = new HashMap<String, byte[]>();
                     	    map1.put(fileimg.getName().replace(".jpg", ""), bytes);

                         
                            // System.out.println(bytes);
                             employee1.setImage(bytes);

                         	}
                 
              }
                  listEmployee.add(employee1);      
            }           
			return listEmployee;
			
}
}                 	 
                    	 
                    	 
                    	 
                    	 
                    	 
                          