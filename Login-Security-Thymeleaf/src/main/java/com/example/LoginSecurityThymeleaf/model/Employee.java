package com.example.LoginSecurityThymeleaf.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="employee")
public class Employee {
	
	
	@Override
	public String toString() {
		return "Employee [emp_Id=" + emp_Id + ", emp_Name=" + emp_Name + ", image=" + image + ", department="
				+ department + "]";
	}

	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long emp_Id;
	 
	public Long getEmp_Id() {
		return emp_Id;
	}

	public void setEmp_Id(Long emp_Id) {
		this.emp_Id = emp_Id;
	}

	public String getEmp_Name() {
		return emp_Name;
	}

	public void setEmp_Name(String emp_Name) {
		this.emp_Name = emp_Name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

//	public Set<RewardType> getRewardType() {
//		return rewardType;
//	}
//
//	public void setRewardType(Set<RewardType> rewardType) {
//		this.rewardType = rewardType;
//	}

	private String emp_Name;
	  //private String reward_Type;
	  //private String dept;
	 @Lob
	public String image;
//	 
//	 @ManyToOne
//		@JoinColumn(name = "R_Id")
//		private RewardType rewardType;
	 
	 @ManyToOne
		@JoinColumn(name = "dept_Id")
		private Department department;
	 
	 @OneToMany(mappedBy = "employee", fetch = FetchType.LAZY,
	            cascade = CascadeType.ALL)
	    private Set<Rewards>rewards;
//	 @ManyToMany
//		@JoinTable(name = "employee_rewards", joinColumns = @JoinColumn(name = "emp_Id"), inverseJoinColumns = @JoinColumn(name = "R_Id"))
//		private Set<RewardType> rewardType = new HashSet<>();
//		  
}