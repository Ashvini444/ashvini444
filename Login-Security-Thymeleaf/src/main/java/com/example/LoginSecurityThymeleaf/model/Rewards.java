package com.example.LoginSecurityThymeleaf.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Rewards {
    
    @Id
    private String Id1;
    //private String emp_Id;

    @OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "R_Id", nullable = false,unique = true)
	private RewardType rewardType;

    
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "emp_Id", nullable = false)
    private Employee employee;

    
}
