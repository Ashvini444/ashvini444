package com.example.LoginSecurityThymeleaf.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
public class RewardType {
   
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Long R_Id;
   private String Reward_Type;
   private Boolean Status;
   private String description;
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public Long getR_Id() {
	return R_Id;
}
public void setR_Id(Long r_Id) {
	R_Id = r_Id;
}
public String getReward_Type() {
	return Reward_Type;
}
public void setReward_Type(String reward_Type) {
	Reward_Type = reward_Type;
}

public Boolean getStatus() {
	return Status;
}
public void setStatus(Boolean status) {
	Status = status;
}

@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "rewardType")
private Rewards rewards;
   
  
   
}
