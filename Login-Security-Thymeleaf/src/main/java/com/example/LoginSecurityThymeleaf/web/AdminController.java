package com.example.LoginSecurityThymeleaf.web;

import java.io.IOException;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.example.LoginSecurityThymeleaf.model.Department;
import com.example.LoginSecurityThymeleaf.model.Employee;
import com.example.LoginSecurityThymeleaf.model.RewardType;
import com.example.LoginSecurityThymeleaf.model.Rewards;
import com.example.LoginSecurityThymeleaf.repo.DepartmentRepo;
import com.example.LoginSecurityThymeleaf.repo.EmployeeRepo;
import com.example.LoginSecurityThymeleaf.repo.RewardTypeRepo;
import com.example.LoginSecurityThymeleaf.service.UserServiceImpl;

@Controller
@RequestMapping("/admin/")
public class AdminController {
	
	
	@Autowired
	private  DepartmentRepo departmentRepo;
	
	//@Autowired
	//private  DepartmentRepo rewardsRepo;
@Autowired
private EmployeeRepo empRepo;
	@Autowired
	private UserServiceImpl service;
	@Autowired
	private  RewardTypeRepo rewardTypeRepo;
	
	@GetMapping("list")
	public String showUpdateForm(@Validated Employee employee,Model model) {
	model.addAttribute("employees",	empRepo.findAll());
	//model.addAttribute("department",departmentRepo.findAll());


		return "index";
	}
	@GetMapping("listEmployees")
	public String ShowEmployeeList(Model model) {
	//List<Department> department=(List<Department>) departmentRepo.findAll();
	model.addAttribute("listEmployees",	empRepo.findAll());
	//model.addAttribute("department",departmentRepo.findAll());

		return "EmployeeTable";
	}
	@GetMapping("dept")
    public String showDepartmentForm(Department department) {
        return "add-Department";
    }
	@GetMapping("departmentlist")
	public String ShowDepartmentList(@Validated Department departmentList,Model model) {
	model.addAttribute("department",	departmentRepo.findAll());
	

		return "DepartmentList";
	}
//	@GetMapping("addemp")
//    public String showEmployeeForm(Employee employee) {
//        return "add-employeess";
//    }
	@GetMapping("/addemp")
	public String showProductNewForm(Model model) {
		model.addAttribute("employee", new Employee());
		model.addAttribute("listDepartment", departmentRepo.findAll());
		return "add-employeess";
	}
	
	@PostMapping("/addemployee")
    public String saveEmployees(Employee employee)
    {
       
        empRepo.save(employee);
        return "redirect:listEmployees";
    }
	@PostMapping("/adddepartment")
    public String saveDepartment(Department dept)
    {
       
        departmentRepo.save(dept);
        return "redirect:departmentlist";
    }
//	@GetMapping("rewards")
//	public String Showrewards(@Validated Rewards rewards,Model model) {
//	model.addAttribute("rewards",	rewardsRepo.findAll());
//
//
//		return "RewardList";
//	}
//	@RequestMapping(value = "/add", method = {RequestMethod.GET,RequestMethod.POST})
	@PostMapping("/add")
    public String saveEmployee( Long emp_Id,String emp_Name,
    	String reward_Type,
    		 String dept,
    		 MultipartFile image) throws IOException
    {
		
		Employee emp=new Employee();
		emp.setEmp_Id(emp_Id);
        emp.setEmp_Name(emp_Name);
        //emp.setReward_Type(reward_Type);
       // emp.setDept(dept);
		emp.setImage(Base64.getEncoder().encodeToString(image.getBytes()));
		empRepo.save(emp);
    	//empService.saveEmployeeToDB(emp_Id, emp_Name,reward_Type, dept,image);
    	return "redirect:list";
    }
	
	@GetMapping("rewardtype")
	public String Showrewardtype(RewardType rewardType,Model model) {
	model.addAttribute("rewardtype",rewardTypeRepo.findAll());
		return "RewardType";
	}
	
	
	@GetMapping("add-reward")
    public String showRewardForm(RewardType rewardType) {
        return "AddReward";
    }
	
     //@RequestMapping(value = "/rewardtype", method = {RequestMethod.GET,RequestMethod.POST})
        @PostMapping("getreward")
        public String saveRewardType(RewardType rt)
        {
           
            rewardTypeRepo.save(rt);
            return "redirect:rewardtype";
        }
//        public String saveReward( Long R_Id,String Reward_Type,String description,
//                Boolean Status)
//            {
//        	service.saveRewardToDB(R_Id,Reward_Type,description, Status);
//                return "redirect:rewardtype";
//            }
//	
        @GetMapping("edit/{emp_Id}")
        public String showUpdateForm(@PathVariable("emp_Id") Long emp_Id, Model model) {
            Employee employee = empRepo.findById(emp_Id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid student Id:" + emp_Id));
            model.addAttribute("employee", employee);
            return "update";
        }
        @GetMapping("delete/{emp_Id}")
        public String deleteEmployee(@PathVariable("emp_Id") Long emp_Id, Model model) {
        	Employee employee = empRepo.findById(emp_Id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid student Id:" + emp_Id));
        	empRepo.delete(employee);
            model.addAttribute("employee", empRepo.findAll());
            return "index";
        }
}
