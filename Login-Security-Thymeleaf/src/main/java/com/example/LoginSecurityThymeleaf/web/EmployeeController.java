package com.example.LoginSecurityThymeleaf.web;


import java.io.IOException;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.example.LoginSecurityThymeleaf.model.Employee;
import com.example.LoginSecurityThymeleaf.repo.EmployeeRepo;
import com.example.LoginSecurityThymeleaf.service.UserServiceImpl;



@Controller
@RequestMapping("/employees/")
public class EmployeeController {
	@Autowired
	private UserServiceImpl userServiceImpl;
	@Autowired
	private  EmployeeRepo employeeRepository;
	
	

	@GetMapping("signup")
	public String showSignUpForm(Employee employee) {
		return "add-employee";
	}

	@GetMapping("list")
	public String showUpdateForm(@Validated Employee employee,Model model) {
	model.addAttribute("employees",	employeeRepository.findAll());
	//model.addAttribute("department",departmentRepo.findAll());


		return "index";
	}
	
//	@RequestMapping(value = "/add", method = {RequestMethod.GET,RequestMethod.POST})
//	//@PostMapping
//    public String saveEmployee( Long emp_Id,String emp_Name,
//    	String reward_Type,
//    		 String dept,
//    		 MultipartFile image)
//    {
//		userServiceImpl.saveEmployeeToDB(emp_Id, emp_Name,reward_Type, dept,image);
//    	return "redirect:list";
//    }
//	@RequestMapping(value = "/add", method = {RequestMethod.GET,RequestMethod.POST})
//	//@PostMapping
//    public String saveEmployee( Long emp_Id,String emp_Name,
//    	String reward_Type,
//    		 String dept,
//    		 MultipartFile image) throws IOException
//    {
//		
//		Employee emp=new Employee();
//		emp.setEmp_Id(emp_Id);
//        emp.setEmp_Name(emp_Name);
//        //emp.setReward_Type(reward_Type);
//       // emp.setDept(dept);
//		emp.setImage(Base64.getEncoder().encodeToString(image.getBytes()));
//		employeeRepository.save(emp);
//    	//empService.saveEmployeeToDB(emp_Id, emp_Name,reward_Type, dept,image);
//    	return "index";
    
    @GetMapping("edit/{emp_Id}")
    public String showUpdateForm(@PathVariable("emp_Id") Long emp_Id, Model model) {
        Employee employee = employeeRepository.findById(emp_Id)
            .orElseThrow(() -> new IllegalArgumentException("Invalid student Id:" + emp_Id));
        model.addAttribute("employee", employee);
        return "update-employee";
    }

   

    @GetMapping("delete/{emp_Id}")
    public String deleteEmployee(@PathVariable("emp_Id") Long emp_Id, Model model) {
    	Employee employee = employeeRepository.findById(emp_Id)
            .orElseThrow(() -> new IllegalArgumentException("Invalid student Id:" + emp_Id));
    	employeeRepository.delete(employee);
        model.addAttribute("employees", employeeRepository.findAll());
        return "index";
    }
}
