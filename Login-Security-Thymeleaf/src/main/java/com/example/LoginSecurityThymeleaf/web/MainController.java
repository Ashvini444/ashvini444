package com.example.LoginSecurityThymeleaf.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.example.LoginSecurityThymeleaf.repo.EmployeeRepo;

@Controller
public class MainController {
	@Autowired
	private EmployeeRepo emprepo;
    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/")
    public String home(Model model) {
    	model.addAttribute("employees", emprepo.findAll());
        return "index";
    }
}