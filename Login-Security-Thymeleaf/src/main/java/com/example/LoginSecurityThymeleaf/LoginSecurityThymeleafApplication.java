package com.example.LoginSecurityThymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoginSecurityThymeleafApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoginSecurityThymeleafApplication.class, args);
	}

}
