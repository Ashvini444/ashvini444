package com.example.LoginSecurityThymeleaf.repo;

import org.springframework.data.repository.CrudRepository;

import com.example.LoginSecurityThymeleaf.model.Employee;



public interface EmployeeRepo extends CrudRepository<Employee, Long> {

}
