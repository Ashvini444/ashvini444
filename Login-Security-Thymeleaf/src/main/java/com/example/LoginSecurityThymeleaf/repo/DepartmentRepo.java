package com.example.LoginSecurityThymeleaf.repo;

import org.springframework.data.repository.CrudRepository;

import com.example.LoginSecurityThymeleaf.model.Department;


public interface DepartmentRepo extends CrudRepository<Department, Long> {

}
