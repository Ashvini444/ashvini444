package com.nts.email.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nts.email.service.EmailService;
@RestController
public class EmailController {
	

	@Autowired
	EmailService emailService;
    @GetMapping(path = "/checkEmail")
    public String getResponse() throws IOException {
			return emailService.validateEmail();
		
		}

}
