
public class OverLoading {

	
		    public void show(char ch)
		    {
		         System.out.println ("You have typed the letter: "+ch);
		    }
		    public void show(int n1, int n2)  
		    {
		         System.out.println("You have typed the letter: "+n1+", and " + n2);
		    }
		}
		class Main
		{
		   public static void main (String args[] )
		   {
		      OverLoading o1 = new OverLoading ();
		       o1.show('A');
		       o1.show( 1, 2 );
		   }
		

	}


